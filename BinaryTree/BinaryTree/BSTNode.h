#pragma once
#include <iostream>
using namespace std;

class BSTNode
{
	public:
		int key;
		BSTNode  *left, *right;
	public:
		BSTNode() { left = right = NULL; };
		BSTNode(int el) { key = el; left = right = NULL; };
		BSTNode(int el, BSTNode *lt, BSTNode *rt) {key = el; left = lt; right = rt;}
		bool isLT(int el) 
		{
			if (key < el) return true;
			else return false;
		};
		bool isGT(int el) 
		{
			if (key > el) return true;
			else return false;
		};
		bool isEQ(int el)
		{
			if (key == el) return true;
			else return false;
		};
		void setKey(int el) { key = el; };
		int getKey() { return key; };
		void visit() { cout << key << " "; };
};

