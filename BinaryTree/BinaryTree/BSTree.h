#pragma once
#include <iostream>
#include "BSTNode.h"
using namespace std;

class BSTree
{	
	public:
		BSTNode * root;
		int numOfElements;
		int *nizZaSortiranje;
		int redniBroj=0; // koristi se kasnije za sortiranje
		BSTree() { root = NULL; numOfElements = 0; };
		~BSTree() { deleteTree(root); };
		void deleteTree(BSTNode *p);
		bool isEmpty() { return root == NULL; };
		void insert(int el);
		bool isInTree(int  el) { return search(el) != NULL; };
		BSTNode *search(int el) { return search(root, el); };
		BSTNode *search(BSTNode *p, int el);
		void balance(int data[], int first, int last);
		void deleteByCopying(int el);
		void deleteByMerging(int  el);
		void preorder() { preorder(root); };
		void inorder() { inorder(root); };
		void postorder() { postorder(root); };
		void inorder(BSTNode *p);
		void preorder(BSTNode *p);
		void postorder(BSTNode *p);
		void breadthFirst();
		void iterativePreorder();
		void iterativeInorder();
		void iterativePostorder();
		void dodajUNiz(BSTNode *p);
		int nodeHeightRecursive(BSTNode *p);
		int nodeHeight(BSTNode *p, int data);
		int getLevelUtil(BSTNode *node, int data, int level);
		int getLevel(BSTNode *node, int data);
		int countGreater(BSTNode *node, int value);
		void printReachable(BSTNode *node, int value);
};

