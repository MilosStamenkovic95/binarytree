// BinaryTree.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "BSTree.h"
#include <iostream>
using namespace std;

int main()
{
	BSTree *binarnoDrvo = new BSTree();
	binarnoDrvo->insert(1);
	binarnoDrvo->insert(10);
	binarnoDrvo->insert(100);
	binarnoDrvo->insert(20);
	binarnoDrvo->insert(200);
	binarnoDrvo->insert(80);
	binarnoDrvo->insert(800);
	binarnoDrvo->insert(45);
	binarnoDrvo->insert(450);
	binarnoDrvo->insert(30);
	cout << "Inorder ";
	binarnoDrvo->inorder();
	cout << "\n";
	cout << "Preorder ";
	binarnoDrvo->preorder();
	cout << "\n";
	cout << "Postorder ";
	binarnoDrvo->postorder();
	cout << "\n\n";
	cout << "Sad ce ga izbalansiramo";
	cout << "\n\n";
	binarnoDrvo->nizZaSortiranje = new int[binarnoDrvo->numOfElements];
	binarnoDrvo->dodajUNiz(binarnoDrvo->root);
	BSTree *novoDrvo = new BSTree();
	novoDrvo->balance(binarnoDrvo->nizZaSortiranje, 0, binarnoDrvo->numOfElements);
	cout << "Inorder ";
	novoDrvo->inorder();
	cout << "\n";
	cout << "Preorder ";
	novoDrvo->preorder();
	cout << "\n";
	cout << "Postorder ";
	novoDrvo->postorder();
	cout << "\n\n";
	int visinaCvoraBinarno = binarnoDrvo->nodeHeight(binarnoDrvo->root, 45);
	cout << "Visina zadatog cvora u binarnom drvetu je: " << visinaCvoraBinarno;
	cout << "\n";
	int visinaCvoraNovo = novoDrvo->nodeHeight(novoDrvo->root, 45);
	cout << "Visina zadatog cvora u novom drvetu je: " << visinaCvoraNovo;
	cout << "\n";
	int nivoNovogCvora = novoDrvo->getLevel(novoDrvo->root, 45);
	cout << "Dubina cvora sa zadatom vrednoscu je: " << nivoNovogCvora;
	cout << "\n";
	int brojVecih = binarnoDrvo->countGreater(binarnoDrvo->root, 40);
	cout << "Broj cvorova sa vecom od zadate vrednosti ima: " << brojVecih << "\n\n";
	cout << "Od korena mozemo da dodjemo do ovih cvorova za zadati broj koraka: \n";
	binarnoDrvo->printReachable(binarnoDrvo->root, 1);
	binarnoDrvo->deleteTree(binarnoDrvo->root);
	novoDrvo->deleteTree(novoDrvo->root);
	return 0;
}