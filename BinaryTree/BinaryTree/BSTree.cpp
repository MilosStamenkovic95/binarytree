#include "stdafx.h"
#include "BSTree.h"
#include <stack>
#include <queue>
#include <algorithm>

void BSTree::deleteTree(BSTNode *p)
{
	if (p != NULL) 
	{
		deleteTree(p->left);
		deleteTree(p->right);
		delete p;
	}
}

void BSTree::insert(int el)
{
	BSTNode *p = root, *prev = NULL;
	while (p != NULL) 
	{	
		// trazenje mesta za umetanje novog cvora
		prev = p;
		if (p->isLT(el))
		{
			p = p->right;
		}		
		else
		{
			p = p->left;
		}
	}
	if (root == NULL)
	{
		// stablo je prazno
		root = new BSTNode(el);
	}
	else if (prev->isLT(el))
	{
		prev->right = new BSTNode(el);
	}		
	else
	{
		prev->left = new BSTNode(el);
	}
	numOfElements++;
}

BSTNode * BSTree::search(BSTNode *p, int el)
{
	while (p != NULL)
	{
		if (p->isEQ(el))
		{
			return p;
		}
		else if (p->isGT(el))
		{
			p = p->left;
		}
		else
		{
			p = p->right;
		}
	}
	return NULL;
}

void BSTree::balance(int data[], int first, int last)
{
	if (first <= last) 
	{
		if (first == last && last == numOfElements)
		{
			return;
		}
		int middle = ((first + last) / 2);
		insert(data[middle]);
		balance(data, first, middle - 1);
		balance(data, middle + 1, last);
	}
}

void BSTree::deleteByCopying(int el)
{
	BSTNode *node, *p = root, *prev = NULL;
	while (p != NULL && !p->isEQ(el)) 
	{	
		// nalazenje cvora sa zeljenim el.
		prev = p;
		if (p->isLT(el)) p = p->right;
		else p = p->left;
	}
	node = p;
	if (p != NULL && p->isEQ(el)) 
	{
		if (node->right == NULL)
		{
			// cvor nema desnog potomka (1)
			node = node->left;
		}
		else if (node->left == NULL)
		{	// cvor nema levog potomka (2)
			node = node->right;
		}
		else 
		{ 
			// cvor ima oba potomka (3)
			BSTNode *tmp = node->left;
			BSTNode *previous = node;
			while (tmp->right != NULL) 
			{	
				// nalazenje krajnjeg desnog cvora
				previous = tmp; // u levom podstablu
				tmp = tmp->right;
			}
			node->setKey(tmp->getKey()); // prepisivanje reference na kljuc
			if (previous == node)
			{	
				// tmp je direktni levi potomak node-a
				previous->left = tmp->left;
				// ostaje isti raspored u levom podstablu
			}	
			else previous->right = tmp->left; // levi potomak tmp-a
			delete tmp; // se pomera na mesto tmp-a
			numOfElements--;
			return;
		}
		if (p == root)
		{	
			// u slucaju (1) i (2) samo je pomerena
			root = node; // referenca node pa je potrebno
		}
		else if (prev->left == p)
		{	
			// izmeniti i link prethodnog cvora
			prev->left = node; // u slucaju (3) ovo nema dejstva
		}
		else
		{
			prev->right = node;
		}
		delete p;
		numOfElements--;
	}
	else if (root != NULL) throw new exception("Element is not in the tree!");
	else throw new exception("The tree is empty!");
}

void BSTree::deleteByMerging(int el)
{
	BSTNode *tmp, *node, *p = root, *prev = NULL;
	while (p != NULL && !p->isEQ(el)) 
	{	
		// nalazenje cvora sa zeljenim el.
		prev = p;
		if (p->isLT(el)) p = p->right;
		else p = p->left;
	}
	node = p;
	if (p != NULL && p->isEQ(el)) 
	{
		if (node->right == NULL)
		{
			// cvor nema desnog potomka (1)
			node = node->left;
		}
		else if (node->left == NULL)
		{
			// cvor nema levog potomka (2)
			node = node->right;
		}
		else 
		{	
			// cvor ima oba potomka (3)
			tmp = node->left;
			while (tmp->right != NULL)
			{
				// nalazenje krajnjeg desnog cvora
				tmp = tmp->right; // u levom podstablu
			}
			tmp->right = node->right; // prebacivanje desnog linka node-a u tmp
			node = node->left; // na tekucem mestu bice prvi levi potomak
		} 
		if (p == root)
		{
			root = node;
		}		
		else if (prev->left == p)
		{
			prev->left = node;
		}		
		else prev->right = node;
		delete p;
		numOfElements--;
	}
	else if (root != NULL) throw new exception("Element is not in the tree!");
	else throw new exception("The tree is empty!");
}

void BSTree::inorder(BSTNode *p)
{
	if (p != NULL) 
	{
		inorder(p->left);
		p->visit();
		inorder(p->right);
	}
}

void BSTree::preorder(BSTNode *p)
{
	if (p != NULL) 
	{
		p->visit();
		preorder(p->left);
		preorder(p->right);
	}
}

void BSTree::postorder(BSTNode *p)
{
	if (p != NULL) 
	{
		postorder(p->left);
		postorder(p->right);
		p->visit();
	}
}

void BSTree::breadthFirst()
{
	BSTNode *p = root;
	queue <BSTNode*> noviQueue;
	if (p != NULL) 
	{
		noviQueue.push(p);
		//push je ekvivalentan sa enQueue
		while (!noviQueue.empty()) 
		{
			p = noviQueue.front();
			noviQueue.pop();
			//prethodne dve operacije su ekvivalentne p = noviQueue.deQueue() samo sto deQueue ne postoji u sistemski stack u C++
			p->visit();
			if (p->left != NULL)
				noviQueue.push(p->left);
			if (p->right != NULL)
				noviQueue.push(p->right);
		}
	}
}

void BSTree::iterativePreorder()
{
	BSTNode *p = root;
	stack <BSTNode*> noviStack;
	if (p != NULL) 
	{
		noviStack.push(p);
		while (!noviStack.empty()) 
		{
			p = noviStack.top();
			noviStack.pop();
			p->visit();
			if (p->right != NULL) // levi potomak se stavlja u magacin
				noviStack.push(p->right); // posle desnog, da bi se prvi obradio
			if (p->left != NULL)
				noviStack.push(p->left);
		}
	}
}

void BSTree::iterativeInorder()
{
	BSTNode *p = root;
	stack <BSTNode*> noviStack;
	while (p != NULL) 
	{
		while (p != NULL) 
		{
			if (p->right != NULL)
			{
				noviStack.push(p->right); // u magacin se stavlja prvo desni
			}				
			noviStack.push(p); // pa zatim tekuci cvor
			p = p->left; // i prelazi na levog potomka
		}
		p = noviStack.top(); // cvor bez levih potomaka
		noviStack.pop();
		while (!noviStack.empty() && p->right == NULL) 
		{
			p->visit(); // obilazak tekuceg cvora i ostalih
			p = noviStack.top(); // bez desnih potomaka
			noviStack.pop();
		}
		p->visit(); // obilazak prvog cvora
		if (!noviStack.empty())
		{
			// i njegovih desnih potomaka
			p = noviStack.top();
			noviStack.pop();
		}
		else p = NULL;
	}
}

void BSTree::iterativePostorder()
{
	BSTNode *p = root, *q = root;
	stack <BSTNode*> noviStack;
	while (p != NULL) 
	{
		for (; p->left != NULL; p = p->left)
		{
			noviStack.push(p);
		}
		while (p != NULL && (p->right == NULL || p->right == q))
		{
			p->visit();
			q = p;
			if (noviStack.empty())
			{
				return;
			}				
			p = noviStack.top();
			noviStack.pop();
		}
		noviStack.push(p);
		p = p->right;
	}
}

void BSTree::dodajUNiz(BSTNode * p)
{
	if (p != NULL)
	{
		dodajUNiz(p->left);
		nizZaSortiranje[redniBroj++] = p->key;
		dodajUNiz(p->right);
	}
}

int BSTree::nodeHeightRecursive(BSTNode * p)
{
	if (p == NULL)
		return 0;
	else
	{
		/* compute the depth of each subtree */
		int lDepth = nodeHeightRecursive(p->left);
		int rDepth = nodeHeightRecursive(p->right);

		/* use the larger one */
		if (lDepth > rDepth)
			return(lDepth + 1);
		else return(rDepth + 1);
	}
}

int BSTree::nodeHeight(BSTNode * p, int data)
{
	BSTNode *trazeniCvor = search(root, data);
	int trazenaVisina = nodeHeightRecursive(trazeniCvor);
	return trazenaVisina;
}

int BSTree::getLevelUtil(BSTNode * node, int data, int level)
{
	if (node == NULL)
		return 0;

	if (node->key == data)
		return level;

	int downlevel = getLevelUtil(node->left, data, level + 1);
	if (downlevel != 0)
		return downlevel;

	downlevel = getLevelUtil(node->right, data, level + 1);
	return downlevel;
}

int BSTree::getLevel(BSTNode * node, int data)
{
	return getLevelUtil(node, data, 0);
}

int BSTree::countGreater(BSTNode * node, int value)
{
	if (node == NULL)
		return 0;
	int brojac = 0;
	if (node->key > value)
		brojac++;
	brojac += countGreater(node->left, value);
	brojac += countGreater(node->right, value);
}

void BSTree::printReachable(BSTNode * node, int value)
{
	while (value > 0)
	{
		if (node != NULL)
		{
			node->visit();
			printReachable(node->left, value-1);
			printReachable(node->right, value-1);
		}
		value--;
	}
}
